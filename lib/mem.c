/* This file is part of pam-modules.
   Copyright (C) 2008-2022 Sergey Poznyakoff
 
   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License along
   with this program.  If not, see <http://www.gnu.org/licenses/>.  */

#include <graypam.h>

void *
gray_2nrealloc(void *ptr, size_t *pcount, size_t elsiz)
{
	size_t count = *pcount;

	if (!ptr) {
		if (!count) {
			count = 64 / elsiz;
			count += !count;
		}
	} else {
		if ((size_t)-1 / 3 * 2 / elsiz <= count) {
			errno = ENOMEM;
			return NULL;
		}
		count += (count + 1) / 2;
	}
	ptr = realloc(ptr, count * elsiz);
	if (ptr)
		*pcount = count;
	return ptr;
}

void
gray_pam_delete(char *x)
{
	PAM_OVERWRITE(x);
	free(x);
}

void
gray_cleanup_string(pam_handle_t *pamh, void *x, int error_status)
{
	gray_pam_delete(x);
}

void
gray_cleanup_regex(pam_handle_t *pamh, void *x, int error_status)
{
	regfree((regex_t*)x);
}

void
gray_make_str(pam_handle_t *pamh, const char *str, const char *name,
	      char **ret)
{
	int retval;
	char *newstr = XSTRDUP(str);

	retval = pam_set_data(pamh, name, (void *)newstr, gray_cleanup_string);
	if (retval != PAM_SUCCESS) {
		_pam_log(LOG_CRIT, 
			 "can't keep data [%s]: %s",
			 name,
			 pam_strerror(pamh, retval));
		gray_pam_delete(newstr);
	} else {
		*ret = newstr;
		newstr = NULL;
	}
}


