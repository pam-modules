.\" This file is part of PAM-Modules -*- nroff -*-
.\" Copyright (C) 2018-2022 Sergey Poznyakoff
.\"
.\" PAM-Modules is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" PAM-Modules is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with PAM-Modules.  If not, see <http://www.gnu.org/licenses/>.
.TH PAM_INNETGR 8 "August 12, 2018" "PAM-MODULES" "Pam-Modules User Reference"
.SH NAME
pam_innetgr \- test NIS netgroup match
.SH SYNOPSIS
.nh
.na
\fBpam_innetgr\fR\
 \fBnetgroup=\fINAME\fR\
 [\fBhostname=\fINAME\fR]\
 [\fBdomainname=\fINAME\fR]\
 [\fBnogetdomainname\fR]\
 [\fBnoresolve\fR]\
 [\fBsense=\fISENSE\fR]\
 [\fBdebug\fR[\fB=\fINUMBER\fR]]\
 [\fBwaitdebug\fR]\
 [\fBaudit\fR]
.ad
.hy
.SH DESCRIPTION
The \fBpam_innetgr\fR module checks if the user and current host
match a triple in the NIS netgroup supplied via the \fBnetgroup\fR
argument. It returns success if so, and \fBPAM_AUTH_ERR\fR otherwise.

Another possible return values are: \fBPAM_AUTHINFO_UNAVAIL\fR, if
the username was not supplied or the module was unable to determine
the host or domain name, and \fBPAM_SERVICE_ERR\fR, if a generic error
condition (such as a lack of memory) occurred.

In order to determine host and domain name parts, the following
approach is used. First, the
.BR gethostname (2)
function is called to obtain the hostname part. If the
.BR getdomainname (2)
function is available, it is used to determine the
domain part. If the resulting domain part is NULL or the string "(none)", the
.BR gethostbyname (2)
function is invoked with the hostname as its
argument. The returned name (technically speaking, the \fBh_name\fR
member of the \fBstruct hostent\fR) is used as the canonical name of
the server. It is split on the first occurrence of the dot character.
The second part is used as the domain name. The options described below
control this process.
.SH OPTIONS
.TP
\fBnetgroup=\fISTRING\fR
Name of the netgroup to use. This option is mandatory.
.TP
\fBhostname=\fISTRING\fR
Defines the hostname of the current host. By default it is determined
using the
.BR gethostname (2)
system call.
.TP
\fBdomainname=\fISTRING\fR
Defines the domainname of the current host.
.TP
.B nogetdomainname
Disable the use of
.BR getdomainname (2)
system call.
.sp
Never use this option together \fBnoresove\fR.
.TP
.B noresolve
Don't fallback to obtaining the fully qualified domain name of the
host from DNS in order to obtain the domain part. This means that
if \fBgetdomainname\fR call fails or is not available on your system,
the module will return \fBPAM_SERVICE_ERR\fR.
.sp
Never use this option together with \fBnogetdomainname\fR.
.TP
\fBsense=allow\fR|\fBdeny\fR
What to do if the user name matches the expression given by the
\fBregex\fR option.  The value \fBallow\fR (the default) instructs the
module to return \fBPAM_SUCCESS\fR, the \fBdeny\fR instructs it to
return \fBPAM_AUTH_ERR\fR.
.TP
\fBdebug\fR[\fB=\fINUMBER\fR]
Set debugging level (0 <= \fINUMBER\fR <= 100).
.TP
\fBaudit\fR
Log full debugging information (equivalent to \fBdebug=100\fR).
.TP
\fBwaitdebug=\fIN\fR
Wait for \fIN\fR seconds before starting up.  This option is intended
to facilitate attaching to the module with
.BR gdb (1).
It is available only if the package was configured with
the \fB\-\-enable\-debug\fR option.
.SH MODULE TYPES PROVIDED
All module types (\fBaccount\fR, \fBauth\fR, \fBpassword\fR and
\fBsession\fR) are provided.
.SH RETURN VALUES
.TP
.B PAM_SUCCESS
Successful return.
.TP
.B PAM_AUTH_ERR
Authentication failed.
.TP
.B PAM_AUTHINFO_UNAVAIL
The input information is not sufficient.
.TP
.B PAM_SERVICE_ERR
A generic error condition (such as lack of memory) was encountered.
.TP
.B PAM_USER_UNKNOWN
Supplied username not found.
.SH EXAMPLES
auth required pam_innetgr.so netgroup=grnam
.SH NOTE
This manpage is a short description of \fBpam_innetgr\fR.  For a detailed
discussion, including examples and usage recommendations, refer to the
\fBPAM-modules Manual\fR available in texinfo format.  If the \fBinfo\fR
reader and the tar documentation are properly installed on your
system, the command
.PP
.RS +4
.B info pam-modules
.RE
.PP
should give you access to the complete manual.
.PP
You can also view the manual using the info mode in
.BR emacs (1),
or find it in various formats online at
.PP
.RS +4
.B http://www.gnu.org.ua/software/pam-modules/manual
.RE
.PP
If any discrepancies occur between this manpage and the
\fBPAM-modules Manual\fR, the later shall be considered the authoritative
source.
.SH "SEE ALSO"
.BR pam.conf (5),
.BR pam.d (5),
.BR pam (8).
.SH AUTHORS
Sergey Poznyakoff <gray@gnu.org>
.SH "BUG REPORTS"
Report bugs to <bug\-pam\-modules@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2018 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-functions 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:
